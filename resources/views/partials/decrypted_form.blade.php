<div class="panel panel-default mw800" v-if="showDecryptedForm || {{ (int)($errors->count() && old('key')) }}">
    <header class="panel-heading">
        <h3>Decrypt data</h3>
    </header>
    <div class="panel-body">

        @include('partials.errors')

        <form action="/decrypt" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="file" class="btn btn-info btn-sm">Upload file </label>
                <input type="file" name="file" id="file" class="hide">

                <label for="data">or past encrypted text to decrypt:</label>
                <textarea name="data" id="data" class="form-control mt20" rows="5">
                                        {{ old('data') }}
                                    </textarea>
            </div>

            <div class="form-group">
                <label for="data">Private key:</label>
                <textarea name="key" id="key" class="form-control" rows="5" required>
                                        {{ old('key') }}
                                    </textarea>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Submit">
                <a href="/" class="btn btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>