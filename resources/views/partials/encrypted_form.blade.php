<div class="panel panel-default mw800" v-if="showEncryptedForm || {{ (int)($errors->count() && !old('key')) }}">
    <header class="panel-heading">
        <h3>Encrypt data</h3>
    </header>
    <div class="panel-body">

        @include('partials.errors')

        <form action="/encrypt" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="file" class="btn btn-info btn-sm">Upload file </label>
                <input type="file" name="file" id="file" class="hide">

                <label for="data">or past text to encrypt:</label>
                <textarea name="data" id="data" class="form-control mt20" rows="5">
                                        {{ old('data') }}
                                    </textarea>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Submit">
                <a href="/" class="btn btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>