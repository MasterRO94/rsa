@extends('app')

@section('content')

    <div class="panel panel-success mw800">
        <header class="panel-heading">
            <h3>Encrypted data results</h3>
        </header>
        <div class="panel-body text-left">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="">
                        <a href="#rawData" aria-controls="rawData" role="tab" data-toggle="tab">
                            Raw data
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="#encryptedData" aria-controls="encryptedData" role="tab" data-toggle="tab">
                            Encrypted data
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#publicKey" aria-controls="publicKey" role="tab" data-toggle="tab">
                            Public key
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#privateKey" aria-controls="privateKey" role="tab" data-toggle="tab">
                            Private key
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade" id="rawData">
                        <pre class="center-block maxw520">
                            {{ $data }}
                        </pre>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in active" id="encryptedData">
                        <pre class="center-block maxw520">
                            {{ $encrypted }}
                        </pre>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="publicKey">
                            <pre class="center-block maxw520">
                                {{ $rsa->getPublicKey() }}
                            </pre>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="privateKey">
                        <pre class="center-block maxw520">
                            {{ $rsa->getPrivateKey() }}
                        </pre>
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group">
                <a href="/" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
@endsection
