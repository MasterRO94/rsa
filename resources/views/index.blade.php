@extends('app')

@section('content')

    @include('partials.encrypted_form')
    @include('partials.decrypted_form')

    <template v-if="!showEncryptedForm && !showDecryptedForm && !{{ $errors->count() }}">
        <div class="title m-b-md">
            RSA
        </div>

        <div class="links">
            <a href="#" v-on:click.prevent="showEncryptedForm=true">Encrypt data</a>
            <a href="#" v-on:click.prevent="showDecryptedForm=true">Decrypt data</a>
        </div>
    </template>
@endsection