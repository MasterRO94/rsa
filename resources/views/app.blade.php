<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>RSA</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/css/app.css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100%;
            margin: 0;
            margin-top: 40px;
        }

        .full-height {
            height: 100%;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .mw800 {
            min-width: 800px;
        }

        .maxw520 {
            max-width: 520px;
        }

        .mt10 {
            margin-top: 10px;
        }

        .mt20 {
            margin-top: 20px;
        }

        .tab-pane {
            padding: 7px;
            max-height: 500px;
            overflow-y: auto;
        }

        .errors,
        input,
        textarea,
        .well,
        pre,
        .panel-body {
            list-style: none;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        pre {
            white-space: -moz-pre-wrap; /* Mozilla */
            white-space: -hp-pre-wrap; /* HP printers */
            white-space: -o-pre-wrap; /* Opera 7 */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: pre-wrap; /* CSS 2.1 */
            white-space: pre-line; /* CSS 3 (and 2.1 as well, actually) */
            word-wrap: break-word; /* IE */
            word-break: break-all;
        }

        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body>
<main id="app">
    <div class="flex-center position-ref full-height">
        <div class="content" v-cloak>
            @yield('content')
        </div>
    </div>
</main>


<script src="/js/app.js"></script>
</body>
</html>
