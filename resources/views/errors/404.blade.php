@extends('app')

@section('content')
    <div class="title m-b-md">
        <h1>404 - Page Not Found!</h1>
        <h2><a href="/">Back</a></h2>
    </div>
@endsection