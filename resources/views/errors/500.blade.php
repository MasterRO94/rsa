@extends('app')

@section('content')
    <div class="title m-b-md">
        <h1>Error: {{ $exception->getMessage() }}</h1>
        <h2><a href="/">Back</a></h2>
    </div>
@endsection