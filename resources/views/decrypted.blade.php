@extends('app')

@section('content')

    <div class="panel panel-success mw800">
        <header class="panel-heading">
            <h3>Decrypted data results</h3>
        </header>
        <div class="panel-body text-left">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#decryptedData" aria-controls="encryptedData" role="tab" data-toggle="tab">
                            Decrypted data
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="decryptedData">
                        <pre class="center-block maxw520">
                            {{ $decrypted }}
                        </pre>
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group">
                <a href="/" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
@endsection
