<?php


Route::get('/', function () {
    return view('index');
});

Route::post('/encrypt', 'RSAController@encrypt');
Route::post('/decrypt', 'RSAController@decrypt');