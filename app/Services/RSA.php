<?php

namespace App\Services;

use Exception;

class RSA
{

	protected $public_key;
	protected $private_key;


	/**
	 * RSA constructor.
	 * @param null $private_key
	 * @param null $public_key
	 */
	public function __construct($private_key = null, $public_key = null)
	{
		$this->private_key = $private_key;
		$this->public_key = $public_key;
	}


	/**
	 * @return mixed
	 */
	public function getPublicKey()
	{
		return $this->public_key;
	}


	/**
	 * @param mixed $public_key
	 */
	public function setPublicKey($public_key)
	{
		$this->public_key = $public_key;
	}


	/**
	 * @return mixed
	 */
	public function getPrivateKey()
	{
		return $this->private_key;
	}


	/**
	 * @param mixed $private_key
	 */
	public function setPrivateKey($private_key)
	{
		$this->private_key = $private_key;
	}


	/**
	 * @return resource
	 */
	public function generatePublicKey()
	{
		$certificate = storage_path('app/keys/public.pem');

		return openssl_get_publickey($certificate);
	}


	/**
	 * @param int $bits
	 * @return static|bool
	 */
	public static function generateKeys($bits = 4096)
	{
		$rsa = new static;

		$config = [
			'config' => config('app.ssl_config_path'),
			'digest_alg' => 'sha512',
			'private_key_bits' => (int)$bits,
			'private_key_type' => OPENSSL_KEYTYPE_RSA,
		];

		$res = openssl_pkey_new($config);

		openssl_pkey_export($res, $private_key, null, $config);

		$rsa->setPrivateKey($private_key);

		$public_key_data= openssl_pkey_get_details($res);
		if ($public_key_data === false) return false;

		$public_key = array_get($public_key_data, 'key');

		$rsa->setPublicKey($public_key);

		return $rsa;
	}


	/**
	 * @param $data
	 * @return string
	 * @throws Exception
	 */
	public function encrypt($data)
	{
		if (!openssl_public_encrypt($data, $encrypted, $this->public_key)) {
			throw new Exception('Unable to encrypt data. Perhaps it is bigger than the key size?');
		}

		return base64_encode($encrypted);
	}


	/**
	 * @param $data
	 * @return string
	 * @throws Exception
	 */
	public function decrypt($data)
	{
		if (!openssl_private_decrypt(base64_decode($data), $decrypted, $this->private_key)) {
			throw new Exception('Unable to decrypt data.');
		}

		return $decrypted;
	}
}