<?php

namespace App\Http\Controllers;

use File;
use App\Services\RSA;
use Illuminate\Http\Request;

class RSAController extends Controller
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function encrypt(Request $request)
	{
		$this->validate($request, [
			'file' => 'mimes:txt|max:4096|required_without:data',
			'data' => 'min:2|max:4096|required_without:file'
		]);

		$rsa = RSA::generateKeys();

		$data = $this->data($request);

		$encrypted = $rsa->encrypt($data);

		return view('encrypted', compact('rsa', 'data', 'encrypted'));
	}


	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function decrypt(Request $request)
	{
		$this->validate($request, [
			'file' => 'mimes:txt|max:4096|required_without:data',
			'data' => 'min:2|max:4096|required_without:file',
			'key' => 'required',
		]);

		$rsa = new RSA($request->get('key'));

		$data = $this->data($request);

		$decrypted = $rsa->decrypt($data);

		return view('decrypted', compact('data', 'rsa', 'decrypted'));
	}


	/**
	 * @param Request $request
	 * @return mixed
	 */
	protected function data(Request $request)
	{
		if ($request->file('file')) {
			$file = $request->file->store('temp');
			$data = File::get(storage_path("app/$file"));
			File::delete(storage_path("app/$file"));
		} else {
			$data = $request->get('data');
		}

		return $data;
	}
}
